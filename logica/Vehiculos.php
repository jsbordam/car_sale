<?php
    //Para importar las clases que necesito usar:
    require "persistencia/VehiculosDAO.php";
    
    class Vehiculos 
    {
        private $id_vehiculo;
        private $id_marca;
        private $id_color;
        private $modelo;
        private $precio;
        private $foto;

        private $conexion;
        private $vehiculosDAO;
     
        
        //Constructor:
        
        function Vehiculos($pId_vehiculo="", $pId_marca="", $pId_color="", $pModelo="",  $pPrecio="",  $pFoto="") 
        {
            $this -> id_vehiculo = $pId_vehiculo;
            $this -> id_marca = $pId_marca;
            $this -> id_color = $pId_color;
            $this -> modelo = $pModelo;
            $this -> precio = $pPrecio;
            $this -> foto = $pFoto;

            $this -> conexion = new Conexion();
            $this -> vehiculosDAO = new VehiculosDAO($pId_vehiculo, $pId_marca, $pId_color, $pModelo, $pPrecio, $pFoto);
        }
        
        
    
        
        //Metodos GET:
        
        public function getId_vehiculo()
        {
            return $this->id_vehiculo;
        }
        
        public function getId_marca()
        {
            return $this->id_marca;
        }

        public function getId_color()
        {
            return $this->id_color;
        }

        public function getModelo()
        {
            return $this->modelo;
        }

        public function getPrecio()
        {
            return $this->precio;
        }

        public function getFoto()
        {
            return $this->foto;
        }
        
        
             
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un registro:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> crear();
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion      
        }
        
        
        //Metodo para consultar todos los registros:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        }

        //Metodo para buscar por palabra clave:
        function buscar($filtro)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> buscar($filtro);
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> buscar($filtro));
            $this -> conexion -> cerrar();
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        } 

        //Metodo para buscar por un solo filtro (marca o modelo):
        function buscarFiltro($selector, $filtro)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> buscarFiltro($selector, $filtro);
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> buscarFiltro($selector, $filtro));
            $this -> conexion -> cerrar();
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        } 

        //Metodo para buscar por un solo filtro (precio):
        function buscarFiltroPre($filtro)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> buscarFiltroPre($filtro);
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> buscarFiltroPre($filtro));
            $this -> conexion -> cerrar();
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        } 

        //Metodo para buscar por 2 filtros (marca y modelo):
        function buscarFiltro2($filtro1, $filtro2)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> buscarFiltro2($filtro1, $filtro2);
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> buscarFiltro2($filtro1, $filtro2));
            $this -> conexion -> cerrar();
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        }

        //Metodo para buscar por 2 filtros (marca y precio):
        function buscarFiltro2Pre($selector,$filtro1, $filtro2)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> buscarFiltro2Pre($selector,$filtro1, $filtro2);
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> buscarFiltro2Pre($selector,$filtro1, $filtro2));
            $this -> conexion -> cerrar();
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        }


        //Metodo para buscar por marca, modelo y precio:
        function buscarFiltro3($filtro1, $filtro2, $filtro3)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> vehiculosDAO -> buscarFiltro3($filtro1, $filtro2, $filtro3);
            $this -> conexion -> ejecutar($this -> vehiculosDAO -> buscarFiltro3($filtro1, $filtro2, $filtro3));
            $this -> conexion -> cerrar();
            $vehiculos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($vehiculos, new Vehiculos ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $vehiculos;
        }
    }
?>