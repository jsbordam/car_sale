<?php
    class VehiculosDAO
    {
        private $id_vehiculo;
        private $id_marca;
        private $id_color;
        private $modelo;
        private $precio;
        private $foto;
     
        
        //Constructor:
        
        function VehiculosDAO($pId_vehiculo="", $pId_marca="", $pId_color="", $pModelo="",  $pPrecio="",  $pFoto="") 
        {
            $this -> id_vehiculo = $pId_vehiculo;
            $this -> id_marca = $pId_marca;
            $this -> id_color = $pId_color;
            $this -> modelo = $pModelo;
            $this -> precio = $pPrecio;
            $this -> foto = $pFoto;
        }

        
        //Metodos para las consultas en la BD:
         
        //Para crear:
        function crear()
        {
            return "INSERT into vehiculos (id_marca, id_color, modelo, precio, foto)
                    VALUES ('" . $this -> id_marca . "', '" . $this -> id_color . "', '" . $this -> modelo . "', '" . $this -> precio . "', '" . $this -> foto . "')";
        }      
        

        //Para consultar todos los vehiculos:
        function consultarTodos()
        {
            return "SELECT id_vehiculo, id_marca, id_color, modelo, precio, foto
                    FROM vehiculos";
        }

        //Para buscar por palabra clave (marca):
        function buscar($filtro)
        {
            return "SELECT id_vehiculo, v.id_marca, id_color, modelo, precio, foto
                    FROM vehiculos v
                    INNER JOIN marcas m
                    ON v.id_marca = m.id_marca AND  m.marca LIKE '%" . $filtro . "%'";
        }

        //Para buscar por 1 solo filtro (marca o modelo):
        function buscarFiltro($selector, $filtro)
        {
            return "SELECT id_vehiculo, id_marca, id_color, modelo, precio, foto
                    FROM vehiculos
                    WHERE " . $selector . " = '" . $filtro . "'";
        }

        //Para buscar por 1 solo filtro (precio):
        function buscarFiltroPre($filtro)
        {
            //Para identificar filtro:
            if ($filtro == "1")
            {
                //Menos de 30'000.000:
                $filtro = "0";
                $filtro2 = "30000000";
            }
            else if ($filtro == "2")
            {
                //30'000.000 - 60'000.000:
                $filtro = "30000000";
                $filtro2 = "60000000";
            }
            else if ($filtro == "3")
            {
                //61'000.000 - 100'000.000:
                $filtro = "61000000";
                $filtro2 = "100000000";
            }
            else
            {
                //Más de 100'000.000:
                $filtro = "100000000";
                $filtro2 = "300000000";
            }


            return "SELECT id_vehiculo, id_marca, id_color, modelo, precio, foto
                    FROM vehiculos
                    WHERE precio BETWEEN '"  . $filtro . "' AND '" . $filtro2 . "'";
        }

        //Para buscar por 2 filtros (marca, modelo):
        function buscarFiltro2($filtro1, $filtro2)
        {
            return "SELECT id_vehiculo, id_marca, id_color, modelo, precio, foto
                    FROM vehiculos
                    WHERE id_marca = '" . $filtro1 . "' AND modelo = '" . $filtro2 . "'";
        }

        //Para buscar por filtro (marca, precio):
        function buscarFiltro2Pre($selector,$filtro1, $filtro2)
        {
            //Para identificar filtro2:
            if ($filtro2 == "1")
            {
                //Menos de 30'000.000:
                $filtro2 = "0";
                $filtro3 = "30000000";
            }
            else if ($filtro2 == "2")
            {
                //30'000.000 - 60'000.000:
                $filtro2 = "30000000";
                $filtro3 = "60000000";
            }
            else if ($filtro2 == "3")
            {
                //61'000.000 - 100'000.000:
                $filtro2 = "61000000";
                $filtro3 = "100000000";
            }
            else
            {
                //Más de 100'000.000:
                $filtro2 = "100000000";
                $filtro3 = "300000000";
            }


            return "SELECT id_vehiculo, id_marca, id_color, modelo, precio, foto
                    FROM vehiculos
                    WHERE " . $selector . " = '" . $filtro1 . "' AND precio BETWEEN '"  . $filtro2 . "' AND '" . $filtro3 . "'";
        }

        //Para buscar por filtro (marca, modelo, precio):
        function buscarFiltro3($filtro1, $filtro2, $filtro3)
        {
            //Para identificar filtro3:
            if ($filtro3 == "1")
            {
                //Menos de 30'000.000:
                $filtro3 = "0";
                $filtro4 = "30000000";
            }
            else if ($filtro3 == "2")
            {
                //30'000.000 - 60'000.000:
                $filtro3 = "30000000";
                $filtro4 = "60000000";
            }
            else if ($filtro3 == "3")
            {
                //61'000.000 - 100'000.000:
                $filtro3 = "61000000";
                $filtro4 = "100000000";
            }
            else
            {
                //Más de 100'000.000:
                $filtro3 = "100000000";
                $filtro4 = "300000000";
            }

            return "SELECT id_vehiculo, id_marca, id_color, modelo, precio, foto
                    FROM vehiculos
                    WHERE id_marca = '" . $filtro1 . "' AND modelo = '" . $filtro2 . "' AND precio BETWEEN '"  . $filtro3 . "' AND '" . $filtro4 . "'";
        }         
    }
?>
