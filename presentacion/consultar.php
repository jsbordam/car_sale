<?php 
    //Se consultan todos los vehiculos que estan registrados en la BD:

    $vehiculos = new Vehiculos();
    $arrayVehiculos = $vehiculos -> consultarTodos();
?>


<div class="sidebar">
    <!-- Sidebar  -->
    <nav id="sidebar">

        <div id="dismiss">
            <i class="fa fa-arrow-left"></i>
        </div>

        <ul class="list-unstyled components">

            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
                    <div align="center" style="font-size:25px;"><i class="fas fa-home"></i></div>
                </a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/registrar.php")?>" align="center">Registra
                    tu auto</a>
            </li>
            <li class="active">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/consultar.php")?>" align="center">Catalogo
                    de autos</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/comentarios.php")?>"
                    align="center">Comentarios</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/contacto.php")?>"
                    align="center">Contacto</a>
            </li>
        </ul>

    </nav>
</div>


<div id="content">
    <section id="portfolio" class="dark_bg_orange layout_padding cross_layout_o padding_top_0">
        <div class="container">
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo"> <a
                                            href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img
                                                src="https://moodle.com/wp-content/uploads/2019/03/edulabs.png"
                                                width="400px" alt="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="right_header_info">
                                <ul>
                                    <li>
                                        <button type="button" id="sidebarCollapse">
                                            <img src="images/menu_icon.png" alt="#" />
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="full center">
                        <h2 class="heading_main orange_heading">CATALOGO</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" align="center">
                    <div class="card2">
                        <div class="row">
                            <div class="col-md-3" align="right">
                                <div class="card-header">
                                    <font face="Arial Black" size="4"> <i class="fas fa-search"></i>
                                        Buscar
                                    </font>
                                </div>
                            </div>

                            <div class="col-md-9" align="left">
                                <div class="card-header">
                                    <div class="form-group">
                                        <input type="text" id="filtro" class="form-control" placeholder="Palabra clave">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <br>

            <div class="form-row">
                <div class="form-group col-sm-4">
                    <input type="checkbox" name="checks[]" value="1">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Marca de vehiculo:</label>
                        </div>
                        <select class="custom-select" id="filterMarca">
                            <option value="1">Mazda</option>
                            <option value="2">Ford</option>
                            <option value="3">Chevrolet</option>
                            <option value="4">BMW</option>
                            <option value="5">Renault</option>
                        </select>
                    </div>
                </div>

                <div class="form-group col-sm-4">
                    <input type="checkbox" name="checks[]" value="2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Modelo (año):</label>
                        </div>
                        <select class="custom-select" id="filterModelo">
                            <option value="2021">2021</option>
                            <option value="2020">2020</option>
                            <option value="2019">2019</option>
                            <option value="2018">2018</option>
                            <option value="2017">2017</option>
                        </select>
                    </div>
                </div>

                <div class="form-group col-sm-4">
                    <input type="checkbox" name="checks[]" value="3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Precio:</label>
                        </div>
                        <select class="custom-select" id="filterPrecio">
                            <option value="1">Menos de 30'000.000</option>
                            <option value="2">30'000.000 - 60'000.000</option>
                            <option value="3">61'000.000 - 100'000.000</option>
                            <option value="4">Más de 100'000.000</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <input type="submit" id="aplicar" name="aplicar" value="Aplicar" class="btn login_btn2 btn-block">
                </div>
            </div>

            <br>

            <div class="row" id="resultados">
                <?php		     
    			foreach ($arrayVehiculos as $vehiculoActual)
    			{   			   
                    //Para identificar marca:
                    if ($vehiculoActual -> getId_marca() == '1') 
                    {
                        $marca = "Mazda";
                    }
                    else if ($vehiculoActual -> getId_marca() == '2')
                    {
                        $marca = "Ford";
                    }
                    else if ($vehiculoActual -> getId_marca() == '3')
                    {
                        $marca = "Chevrolet";
                    }
                    else if ($vehiculoActual -> getId_marca() == '4')
                    {
                        $marca = "BMW";
                    }
                    else
                    {
                        $marca = "Renault";
                    }

                    echo "<div class='col-md-3'>";
                        echo "<br>";
                        echo "<div class='card'>";
                            
                            echo "<a class='portfolio-item' style='background-image: url(" . $vehiculoActual -> getFoto() . ");' href='#'>";
                                echo "<div class='details'>";
                                    echo "<h4 class='card-title'>";
                                        echo "<font face='Algerian'>" .$vehiculoActual -> getId_vehiculo() . "</font>";
                                    echo "</h4>";
                                    echo "<h4 class='card-title'>";
                                        echo "<font face='Algerian'>" . $marca . "</font>";
                                    echo "</h4>";
                                    echo "<br><br><br>";                                                                                       
                                echo "</div>";
                            echo "</a>";
                            echo "<div class='card-body'>";
                                echo "<h4 class='card-title'>";
                                        echo "<font face='Algerian'>" . $marca . " " . $vehiculoActual -> getModelo() . "</font>";
                                echo "</h4>";
                                echo "<h3 class='card-text'";
                                echo "<font face='Arial'>$" . number_format($vehiculoActual -> getPrecio(), ...array(0, ',', '.')) . "</font>";
                                echo "</h3>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";   					    	  
    			}
    	    ?>
            </div>
        </div>
    </section>
</div>



<!-- Scrollbar Js Files -->


<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    /////////////////////////////////////////////////////////////////////////////////////////
    //Este es el JQuery para buscar Vehiculos por palabra clave: 

    $("#filtro").keyup(function() 
    {
        if ($("#filtro").val().length >= 0) 
        {
            //Seteamos los checkbox y los selectores:
            $('input[type="checkbox"]').prop('checked' , false);
            
            //Encriptamos la ruta para que no sea visible en la inspección de codigo:
            url =
                "indexAjax.php?pid=<?php echo base64_encode("presentacion/buscarAjax.php") ?>&filtro=" +
                $("#filtro").val();
            $("#resultados").load(url);
        }
    });


    /////////////////////////////////////////////////////////////////////////////////////////
    //Este es el JQuery para buscar Vehiculos por filtros:


    //Funcion flechita para filtrar por un campo:
    const filtro_1_campo = ($selector=false, $filter, $precio=false) => 
    {
        if ($precio) 
        {
            //Encriptamos la ruta para que no sea visible en la inspección de codigo:
            url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/filtrarAjax.php") ?>&opc=1&precio&filter=" 
            + $filter;
        }
        else
        {
            //Encriptamos la ruta para que no sea visible en la inspección de codigo:
            url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/filtrarAjax.php") ?>&opc=1&selector=" + $selector + "&filter=" 
            + $filter;   
        }   

        $("#resultados").load(url);
    }

    //Funcion flechita para filtrar por dos campo:
    const filtro_2_campos = ($selector=false, $filter1, $filter2, $precio=false) => 
    {
        if ($precio) 
        {
            url ="indexAjax.php?pid=<?php echo base64_encode("presentacion/filtrarAjax.php") ?>&opc=2&precio&selector=" 
            + $selector + "&filter1=" + $filter1 + "&filter2=" + $filter2;
        }
        else
        {
            url ="indexAjax.php?pid=<?php echo base64_encode("presentacion/filtrarAjax.php") ?>&opc=2&filter1=" +
            $filter1 + "&filter2=" + $filter2;  
        }   

        $("#resultados").load(url);
    }

    //Funcion flechita para filtrar por tres campo:
    const filtro_3_campos = ($filter1, $filter2, $filter3) => 
    {
        url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/filtrarAjax.php") ?>&opc=3&filter1=" +
                $filter1 + "&filter2=" + $filter2 + "&filter3=" + $filter3;

        $("#resultados").load(url);
    }
    

    //Cuando le da click en aplicar los filtros:
    $('#aplicar').click(function() 
    {
        //Se setea el campo de buscar:
        $('#filtro').val('');

        //Identificar cuales checkbox estan activos para el filtro:

        var arr = $('[name="checks[]"]:checked').map(function() {
        return this.value;
        }).get();

        var str = arr.join(','); //Aquí se guardan los activos

      

        //Para redireccionar a la logica de cada filtro:

        switch (str)
        {
            case '1':
                filtro_1_campo ($selector="id_marca",$("#filterMarca").val(), $precio=false); //FILTRO POR MARCA       
            break;

            case '2':
                filtro_1_campo ($selector="modelo",$("#filterModelo").val()); //FILTRO POR MODELO
            break;    

            case '3':
                filtro_1_campo ($selector=false,$("#filterPrecio").val(), $precio=true); //FILTRO POR PRECIO
            break;

            case '1,2':        
                filtro_2_campos($selector=false,$("#filterMarca").val(), $("#filterModelo").val(),$precio=false); //FILTRO POR MARCA Y MODELO:
            break;    

            case '1,3':        
                filtro_2_campos($selector="id_marca",$("#filterMarca").val(), $("#filterPrecio").val(),$precio=true); //FILTRO POR MARCA Y PRECIO:
            break;

            case '2,3':        
                filtro_2_campos($selector="modelo",$("#filterModelo").val(), $("#filterPrecio").val(),$precio=true); //FILTRO POR MODELO Y PRECIO:
            break;

            case '1,2,3':        
                filtro_3_campos($("#filterMarca").val(),$("#filterModelo").val(), $("#filterPrecio").val()); //FILTRO POR MARCA, MODELO Y PRECIO:
            break;

            case '':        
                //Cuando no selecciona ningún filtro:
                
                url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/filtrarAjax.php") ?>";
                $("#resultados").load(url);
            break;
        }

        
    });
});
</script>


<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>