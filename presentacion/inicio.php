
<div class="wrapper">
    <div class="sidebar">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <div id="dismiss">
                <i class="fa fa-arrow-left"></i>
            </div>

            <ul class="list-unstyled components">

                <li class="active">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
                        <div align="center" style="font-size:25px;"><i class="fas fa-home"></i></div>
                    </a>
                </li>
                <li>
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/registrar.php")?>"
                        align="center">Registra tu auto</a>
                </li>
                <li>
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/consultar.php")?>" align="center">Catalogo de autos</a>
                </li>
                <li>
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/comentarios.php")?>" align="center">Comentarios</a>
                </li>
                <li>
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/contacto.php")?>" align="center">Contacto</a>
                </li>
            </ul>

        </nav>
    </div>

    <div id="content">
        <!-- section -->
        <section id="home" class="top_section">
            <div class="row">
                <div class="col-lg-12">
                    <!-- header -->
                    <header>
                        <!-- header inner -->
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 logo_section">
                                    <div class="full">
                                        <div class="center-desk">
                                            <div class="logo"> <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img
                                                        src="https://moodle.com/wp-content/uploads/2019/03/edulabs.png"
                                                        width="400px" alt="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="right_header_info">
                                        <ul>
                                            <li><img style="margin-right: 15px;" src="images/phone_icon.png"
                                                    alt="#" /><a href="#">301 322 4761</a></li>
                                            <li><img style="margin-right: 15px;" src="images/mail_icon.png" alt="#" /><a
                                                    href="#">stivborda@gmail.com</a></li>
                                            <li><img src="images/search_icon.png" alt="#" /></li>
                                            <li>
                                                <button type="button" id="sidebarCollapse">
                                                    <img src="images/menu_icon.png" alt="#" />
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>


                    <section>
                        <br><br><br>
                        <div class="container-fluid">
                            <div class="row mt-5">
                                <div class="col-md-5">
                                    <div class="full slider_cont_section">
                                        <h4>BIENVENIDO A</h4>
                                        <h3>EDUCARS</h3>
                                        <p>Esta es una plataforma de venta de autos usados.</p>
                                        <div class="button_section">
                                            <a href="index.php?pid=<?php echo base64_encode("presentacion/registrar.php")?>">Registra tu auto</a>
                                            <a href="index.php?pid=<?php echo base64_encode("presentacion/consultar.php")?>">Catalogo de autos</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div id="slider_main" class="carousel slide" data-ride="carousel">
                                        <!-- The slideshow -->
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src="images/slider_1.png" alt="#" />
                                            </div>
                                            <div class="carousel-item">
                                                <img src="images/slider_2.png" alt="#" />
                                            </div>
                                        </div>
                                        <div class="full center">
                                            <a class="carousel-control-prev" href="#slider_main" data-slide="prev">
                                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                            </a>
                                            <a class="carousel-control-next" href="#slider_main" data-slide="next">
                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- Scrollbar Js Files -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});
</script>