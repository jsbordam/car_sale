<div class="sidebar">
    <!-- Sidebar  -->
    <nav id="sidebar">

        <div id="dismiss">
            <i class="fa fa-arrow-left"></i>
        </div>

        <ul class="list-unstyled components">

            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
                    <div align="center" style="font-size:25px;"><i class="fas fa-home"></i></div>
                </a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/registrar.php")?>" align="center">Registra
                    tu auto</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/consultar.php")?>" align="center">Catalogo de autos</a>
            </li>
            <li class="active">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/comentarios.php")?>" align="center">Comentarios</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/contacto.php")?>" align="center">Contacto</a>
            </li>
        </ul>

    </nav>
</div>


<div id="content">
    <section id="testimonial" class="dark_bg_orange layout_padding cross_layout_o padding_top_0">
        <div class="container">

            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo"> <a
                                            href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img
                                                src="https://moodle.com/wp-content/uploads/2019/03/edulabs.png"
                                                width="400px" alt="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="right_header_info">
                                <ul>
                                    <li>
                                        <button type="button" id="sidebarCollapse">
                                            <img src="images/menu_icon.png" alt="#" />
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <h2 class="heading_main orange_heading">Comentarios</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="full">
                        <div id="testimonial_slider" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                                <li data-target="#testimonial_slider" data-slide-to="0" class="active"></li>
                                <li data-target="#testimonial_slider" data-slide-to="1"></li>
                                <li data-target="#testimonial_slider" data-slide-to="2"></li>
                            </ul>
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="testomonial_section">
                                        <div class="full center">
                                            <div class="client_img">
                                                <img src="images/testimonial.png" alt="#" />
                                            </div>
                                        </div>
                                        <div class="full testimonial_cont text_align_center">
                                            <p><strong>Luis Diaz</strong><br><strong
                                                    class="ornage_color">CLiente</strong><br><i>Es una plataforma muy
                                                        intuitiva y facil de usar, he tenido dos muy buenas experiencias
                                                        comprando este año. Los autos usados de EDUCARS son la mejor opción.
                                                    </i></p>
                                            <div class="full text_align_center margin_top_30">
                                                <img src="images/testimonial_qoute.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- Scrollbar Js Files -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});
</script>