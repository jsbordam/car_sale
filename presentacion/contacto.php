<div class="sidebar">
    <!-- Sidebar  -->
    <nav id="sidebar">

        <div id="dismiss">
            <i class="fa fa-arrow-left"></i>
        </div>

        <ul class="list-unstyled components">

            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
                    <div align="center" style="font-size:25px;"><i class="fas fa-home"></i></div>
                </a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/registrar.php")?>" align="center">Registra
                    tu auto</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/consultar.php")?>" align="center">Catalogo
                    de autos</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/comentarios.php")?>"
                    align="center">Comentarios</a>
            </li>
            <li class="active">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/contactos.php")?>"
                    align="center">Contacto</a>
            </li>
        </ul>
    </nav>
</div>

<div id="content">
    <section id="contact" class="dark_bg_blue layout_padding cross_layout padding_top_0 margin_top_0">
        <div class="container">

            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo"> <a
                                            href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img
                                                src="https://moodle.com/wp-content/uploads/2019/03/edulabs.png"
                                                width="400px" alt="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="right_header_info">
                                <ul>
                                    <li>
                                        <button type="button" id="sidebarCollapse">
                                            <img src="images/menu_icon.png" alt="#" />
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <h2 class="heading_main orange_heading">Contacto</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="full">
                        <div class="contact_form">
                            <form>
                                <fieldset class="row">
                                    <div class="col-md-12">
                                        <div class="full field">
                                            <input type="text" placeholder="Tú nombre" name="name" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="full field">
                                            <input type="email" placeholder="Email" name="email" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="full field">
                                            <input type="text" placeholder="Celular" name="number" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="full field">
                                            <textarea placeholder="Mensaje"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="full center">
                                            <a href="#" class="btn login_btn btn-block">Enviar</a>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="full map_section">
                        <div id="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>
    </section>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- Scrollbar Js Files -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});
</script>

<script>
// This example adds a marker to indicate the position of Bondi Beach in Sydney,
// Australia.
function initMap() 
{
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {
            lat: 4.60971,
            lng: -74.08175
        },
    });

    var image = 'images/location_point.png';
    var beachMarker = new google.maps.Marker({
        position: {
            lat: 4.60971,
            lng: -74.08175
        },
        map: map,
        icon: image
    });
}
</script>
<!-- google map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8eaHt9Dh5H57Zh0xVTqxVdBFCvFMqFjQ&callback=initMap">
</script>
<!-- end google map js -->