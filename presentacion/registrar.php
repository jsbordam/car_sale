<?php 
    $error = 0;

    //Para registro de vehiculos:
    if (isset($_POST["guardar"]))
    {
        //Recojo los datos que llegan por POST del formulario:
        $marca = $_POST["marca"];
        $color = $_POST["color"]; 
        $modelo = $_POST["modelo"];
        $precio = $_POST["precio"];
        
        //////////////////////////////////////////////////////////////////////////////////
        //Para validación del archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
        
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "images/vehiculos/" . time() . ".jpg"; 
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error = 1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        
        if ($error == 0)
        {
            //Se crea el registro en la BD:
            $vehiculo = new Vehiculos ("", $marca, $color, $modelo, $precio, $urlServidor);
            $vehiculo -> crear();
        }     
    }     
?>


<div class="sidebar">
    <!-- Sidebar  -->
    <nav id="sidebar">

        <div id="dismiss">
            <i class="fa fa-arrow-left"></i>
        </div>

        <ul class="list-unstyled components">

            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
                    <div align="center" style="font-size:25px;"><i class="fas fa-home"></i></div>
                </a>
            </li>
            <li class="active">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/registrar.php")?>" align="center">Registra
                    tu auto</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/consultar.php")?>" align="center">Catalogo de autos</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/comentarios.php")?>" align="center">Comentarios</a>
            </li>
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/contacto.php")?>" align="center">Contacto</a>
            </li>
        </ul>

    </nav>
</div>

<div id="content">
    <section id="registrar" class="dark_bg_blue layout_padding cross_layout padding_top_0">
        <div class="container">
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo"> <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img
                                                src="https://moodle.com/wp-content/uploads/2019/03/edulabs.png"
                                                width="400px" alt="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="right_header_info">
                                <ul>
                                    <li>
                                        <button type="button" id="sidebarCollapse">
                                            <img src="images/menu_icon.png" alt="#" />
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="full center">
                        <h2 class="heading_main orange_heading">FORMULARIO DE REGISTRO</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="full">
                        <div class="choose_blog text_align_center">
                            <img src="images/c1_icon.png" />
                            <h4>LOS MEJORES PRECIOS DEL MERCADO</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="full">
                        <div class="choose_blog text_align_center">
                            <img src="images/c2_icon.png" />
                            <h4>EL PAPELEO LO HACEMOS NOSOTROS</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="full">
                        <div class="choose_blog text_align_center">
                            <img src="images/c3_icon.png" />
                            <h4>COMPRA SEGURA Y CON GARANTIA</h4>
                        </div>
                    </div>
                </div>

                <section id="register">
                    <div class="row mt-3 justify-content-xl-center">
                        <div class="card-body">
                            <!-- Para avisar cuando se registra un vehiculo -->
                            <?php
                            if(isset($_POST["guardar"]) && $error == 0)
                            { 
                        ?>
                            <section id="alert2">
                                <div class="alert alert-dismissible fade show" role="alert">
                                    <strong><i class="fas fa-check-circle"></i> Información registrada
                                        con éxito!</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </section>
                            <?php 
                            }
                        ?>

                            <!-- Para reporatar error en el archivo de foto-->
                            <?php
                            if (isset($_POST["guardar"]) && $error == 1)
                            { 
                        ?>
                            <section id="alert1">
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong><i class="fas fa-exclamation-triangle"></i> Error. El archivo
                                        foto debe ser de tipo: (jpg o png)</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </section>
                            <?php 
                            }               
                        ?>

                            <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/registrar.php")?>
                                method="post" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <label for="inputState">Selecciona la Marca:</label> <select id="inputState"
                                            class="form-control" name="marca">
                                            <option value="1">Mazda</option>
                                            <option value="2">Ford</option>
                                            <option value="3">Chevrolet</option>
                                            <option value="4">BMW</option>
                                            <option value="5">Renault</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="inputState">Selecciona el Color:</label> <select id="inputState"
                                            class="form-control" name="color">
                                            <option value="1">Negro</option>
                                            <option value="2">Rojo</option>
                                            <option value="3">Blanco</option>
                                            <option value="4">Gris</option>
                                            <option value="5">Azul</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label>Modelo:</label> <input type="text" name="modelo" class="form-control"
                                            placeholder="Escribe el modelo del auto" required="required">
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label>Precio:</label> <input type="number" name="precio" class="form-control"
                                            placeholder="Escribe el precio del auto" required="required">
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label>Foto:</label>
                                        <div class="form-group">
                                            <input type="file" name="foto" class="form-control-file"
                                                required="required">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" name="guardar" value="Guardar" class="btn login_btn btn-block">
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- Scrollbar Js Files -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});
</script>