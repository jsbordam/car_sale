<?php	    

    if (isset($_GET["opc"]) && $_GET["opc"] == "1") //Selecciono 1 solo filtro
    {
        $filter = $_GET["filter"];
        if (isset($_GET["selector"])) $selector = $_GET["selector"];
        $vehiculos = new Vehiculos();
    
        //Si existe la variable (precio) es porque esta buscando por precio, 
        //sino es porque esta buscando por modelo o marca
   
        $arrayVehiculos =  (isset($_GET["precio"])) ? $vehiculos -> buscarFiltroPre($filter) : $vehiculos -> buscarFiltro($selector,$filter);



        //Primero validamos sí el array esta vacio, es porque no encontro coincidencias:
        if (empty($arrayVehiculos)) 
        {   
            echo "<div class='col-md-12'>";
                echo "<br><br><br><br>";
                echo "<h1 align='center'><b>No se encontraron coincidencias...<b></h1>";
            echo "</div>";
        }
        else
        {
            listar($arrayVehiculos);
        }       
    }
    else if(isset($_GET["opc"]) && $_GET["opc"] == "2") //Selecciono 2 filtros al tiempo
    {
        $fil1 = $_GET["filter1"];
        $fil2 = $_GET["filter2"];
        if (isset($_GET["selector"])) $selector = $_GET["selector"];
        
    
        $vehiculos = new Vehiculos();

        //Si existe la variable (precio) es porque esta buscando por marca y precio, 
        //sino es porque esta buscando por marca y modelo

        $arrayVehiculos = (isset($_GET["precio"])) ? $vehiculos -> buscarFiltro2Pre($selector,$fil1, $fil2) : $vehiculos -> buscarFiltro2($fil1, $fil2);


        //Primero validamos sí el array esta vacio, es porque no encontro coincidencias:
        if (empty($arrayVehiculos)) 
        {   
            echo "<div class='col-md-12'>";
                echo "<br><br><br><br>";
                echo "<h1 align='center'><b>No se encontraron coincidencias...<b></h1>";
            echo "</div>";
        }
        else
        {
            listar($arrayVehiculos);
        }       
    }
    else if(isset($_GET["opc"]) && $_GET["opc"] == "3") //Selecciono tres filtros al tiempo
    {
        $filter1 = $_GET["filter1"];
        $filter2 = $_GET["filter2"];
        $filter3 = $_GET["filter3"];
    
        //Traigo el arreglo de registros asociados a esos filtros:
        $vehiculos = new Vehiculos();
        $arrayVehiculos = $vehiculos -> buscarFiltro3($filter1, $filter2, $filter3);


        //Primero validamos sí el array esta vacio, es porque no encontro coincidencias:
        if (empty($arrayVehiculos)) 
        {   
            echo "<div class='col-md-12'>";
                echo "<br><br><br><br>";
                echo "<h1 align='center'><b>No se encontraron coincidencias...<b></h1>";
            echo "</div>";
        }
        else
        {
            listar($arrayVehiculos);
        }       
    }
    else //No selecciono ningún filtro
    {
        //Traigo el arreglo de todos los registros:
        $vehiculos = new Vehiculos();
        $arrayVehiculos = $vehiculos -> consultarTodos();
        listar($arrayVehiculos);
    }
    
    

    function listar($arrayVehiculos)
    {
        foreach ($arrayVehiculos as $vehiculoActual)
        {   			   
            //Para identificar marca:
            if ($vehiculoActual -> getId_marca() == '1') 
            {
                $marca = "Mazda";
            }
            else if ($vehiculoActual -> getId_marca() == '2')
            {
                $marca = "Ford";
            }
            else if ($vehiculoActual -> getId_marca() == '3')
            {
                $marca = "Chevrolet";
            }
            else if ($vehiculoActual -> getId_marca() == '4')
            {
                $marca = "BMW";
            }
            else
            {
                $marca = "Renault";
            }

            echo "<div class='col-md-3'>";
                echo "<br>";
                echo "<div class='card'>";
                    
                    echo "<a class='portfolio-item' style='background-image: url(" . $vehiculoActual -> getFoto() . ");' href='#'>";
                        echo "<div class='details'>";
                            echo "<h4 class='card-title'>";
                                echo "<font face='Algerian'>" .$vehiculoActual -> getId_vehiculo() . "</font>";
                            echo "</h4>";
                            echo "<h4 class='card-title'>";
                                echo "<font face='Algerian'>" . $marca . "</font>";
                            echo "</h4>";
                            echo "<br><br><br>";                                                                                       
                        echo "</div>";
                    echo "</a>";
                    echo "<div class='card-body'>";
                        echo "<h4 class='card-title'>";
                                echo "<font face='Algerian'>" . $marca . " " . $vehiculoActual -> getModelo() . "</font>";
                        echo "</h4>";
                        echo "<h3 class='card-text'";
                        echo "<font face='Arial'>$" . number_format($vehiculoActual -> getPrecio(), ...array(0, ',', '.')) . "</font>";
                        echo "</h3>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";   					    	  
        }
    }
?>


