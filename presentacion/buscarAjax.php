<?php	    
    $filtro = $_GET["filtro"];
    
    //Traigo el arreglo de registros asociados a ese filtro:
    $vehiculos = new Vehiculos();
    $arrayVehiculos = $vehiculos -> buscar ($filtro);


    //Primero validamos sí el array esta vacio, es porque no encontro coincidencias:
    if (empty($arrayVehiculos)) 
    {   
        echo "<div class='col-md-12'>";
            echo "<br><br><br><br>";
            echo "<h1 align='center'><b>No se encontraron coincidencias...<b></h1>";
        echo "</div>";
    }
    else
    {
        //Para resaltar el filtro que se esta buscando:
    
        //Convierte toda la cadena a minusculas
        $filtro = strtolower ($filtro);
        //Convierte la primera letra en Mayuscula
        $filtro = ucwords($filtro);
    
        
        foreach ($arrayVehiculos as $vehiculoActual)
        {   			   
            //Para identificar marca:
            if ($vehiculoActual -> getId_marca() == '1') 
            {
                $marca = "Mazda";
            }
            else if ($vehiculoActual -> getId_marca() == '2')
            {
                $marca = "Ford";
            }
            else if ($vehiculoActual -> getId_marca() == '3')
            {
                $marca = "Chevrolet";
            }
            else if ($vehiculoActual -> getId_marca() == '4')
            {
                $marca = "BMW";
            }
            else
            {
                $marca = "Renault";
            }

            echo "<div class='col-md-3'>";
                echo "<br>";
                echo "<div class='card'>";
                    
                    echo "<a class='portfolio-item' style='background-image: url(" . $vehiculoActual -> getFoto() . ");' href='#'>";
                        echo "<div class='details'>";
                            echo "<h4 class='card-title'>";
                                echo "<font face='Algerian'>" .$vehiculoActual -> getId_vehiculo() . "</font>";
                            echo "</h4>";
                            echo "<h4 class='card-title'>";
                                echo "<font face='Algerian'>" . $marca . "</font>";
                            echo "</h4>";
                            echo "<br><br><br>";                                                                                       
                        echo "</div>";
                    echo "</a>";
                    echo "<div class='card-body'>";
                        echo "<h4 class='card-title'>";
                                echo "<font face='Algerian'>" . str_replace($filtro, "<font color='red'>" . $filtro . "</font>", $marca) . " " . $vehiculoActual -> getModelo() . "</font>";
                        echo "</h4>";
                        echo "<h3 class='card-text'";
                        echo "<font face='Arial'>$" . number_format($vehiculoActual -> getPrecio(), ...array(0, ',', '.')) . "</font>";
                        echo "</h3>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";   					    	  
        }
    } 
?>


