<?php
    //Para importar las clases que necesito usar:
    require "logica/Vehiculos.php";
    require "persistencia/Conexion.php";
    
    //Zona horaria en colombia:
    date_default_timezone_set('America/Bogota'); 
    
    //Este algoritmo es para que me muestre los errores
    //en tiempo de ejecución:
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    ////////////////////////////////////////////////////

    //Pid es una variable para redireccionar de una a otra pagina:
    $pid = NULL;
    if (isset($_GET["pid"]))
    {
        //En pid se guarda la ruta decodificada:
        $pid = base64_decode($_GET["pid"]);
    }
?>

<!doctype html>
<html lang="es">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <!-- Esta linea es para implementar: Font Awesome (iconos) -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <!-- Lineas importantes: -->
    <link rel="stylesheet" type="text/css"
        href="//fonts.googleapis.com/css?family=Dancing+Script:700%7CRaleway:300,400,600,700,900">

  


    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">

    <!-- style css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="css/responsive.css">
    
    <!-- fevicon -->
    <link rel="icon" href="" type="image/gif" />
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
    <!-- awesome fontfamily -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Estas lineas son para implementar: jQuery, Popper.js y Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js">
    </script>
    

    <title>EduCars</title>
    <link rel="icon" type="image/png" href="images/icono.png">
</head>

<body class="main-layout">

    <?php 
        //Para redireccionar
        if (isset($pid))
        {
            include $pid;
        }
        else 
        {
            include "presentacion/inicio.php";
        }
    ?>
    
</body>

</html>