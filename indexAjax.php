
<?php  
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();
    require "logica/Vehiculos.php";
    require "persistencia/Conexion.php";
    
    //Desencriptamos el pid para que lo pueda leer:
    $pid = base64_decode($_GET["pid"]);
    include $pid;
?>